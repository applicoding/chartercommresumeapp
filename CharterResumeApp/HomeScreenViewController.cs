﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace CharterResumeApp
{
    public class HomeScreenViewController : UIViewController
    {
        public UITableView table;
        private LoginViewController loginpage;
        private UILabel titleLabel, descriptionLabel;
        public List<User> users = new List<User>();

            public HomeScreenViewController()
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            var width = View.Bounds.Width;
            var height = View.Bounds.Height;
            var margins = View.LayoutMarginsGuide;

            nfloat h = 30;
            titleLabel = new UILabel()
            {
                Text = "Charter Users",
                
                TextColor = UIColor.Blue,
                BackgroundColor = UIColor.SystemBackgroundColor,
                TextAlignment = UITextAlignment.Center,
                Frame = new CGRect(10, 90, width - 20, 50)
            };
            descriptionLabel = new UILabel()
            {
                Text = "Select a user",
                MinimumFontSize = 24,
                TextColor = UIColor.White,
                BackgroundColor = UIColor.SystemBackgroundColor,
                TextAlignment = UITextAlignment.Center,
                Frame = new CGRect(10, 150, width - 20, h)
            };
            titleLabel.Font = titleLabel.Font.WithSize(36);
            table = new UITableView(new CGRect(0, 200, width, height));
            table.AutoresizingMask = UIViewAutoresizing.All;
            CreateTableItems();
            
            View.Add(table);
            View.Add(titleLabel);
            View.Add(descriptionLabel);


            UIBarButtonItem btn = new UIBarButtonItem();
            btn.Image = UIImage.FromBundle("plusbutton.png");
            btn.TintColor = UIColor.Green;
            btn.Clicked += (sender, e) =>
            {
                loginpage = new LoginViewController(this);
                NavigationController.PushViewController(loginpage, true);
            };
            NavigationItem.RightBarButtonItem = btn;
        }

        protected void CreateTableItems()
        {
            //todo convert to json reader
            User user1 = new User()
            {
                Username = "gingerbreadman23",
                Password = "******",
                Id = 1
            }; 
            User user2 = new User()
            {
                Username = "travissteve18",
                Password = "******",
                Id = 2
            }; 
            User user3 = new User()
            {
                Username = "thristydrinker34",
                Password = "******",
                Id = 3
            }; 
            User user4 = new User()
            {
                Username = "horrorplace89",
                Password = "******",
                Id = 4
            };
            

            users.Add(user1);
            users.Add(user2);
            users.Add(user3);
            users.Add(user4);

            table.Source = new TableSource(users, this);
        }

        public override bool PrefersStatusBarHidden()
        {
            return true;
        }
    }
}

#region Maybe Reusable code

//Add(table);
//UIButton myButton = new UIButton(UIButtonType.System);
//myButton.Frame = new CGRect(25, 25, 50, 50);
//myButton.SetTitle("Hello, World!", UIControlState.Normal);
//myButton.SetImage(UIImage.FromBundle("plusbutton.png"), UIControlState.Normal );
//myButton.BackgroundColor = UIColor.Green;
////table.BottomAnchor.ConstraintEqualTo(margins.TopAnchor,10).Active = true;
//myButton.TouchUpInside += (sender, e) => {

//};
//View.Add(myButton);

#endregion