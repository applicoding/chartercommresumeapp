﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CoreGraphics;
using Foundation;
using UIKit;

namespace CharterResumeApp
{
    public class LoginViewController : UIViewController
    {
        UITextField usernameField,passwordField;
        private HomeScreenViewController view;
        public LoginViewController(HomeScreenViewController view)
        {
            this.view = view;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.BackgroundColor = UIColor.White;
            Title = "Add New User";

            View.BackgroundColor = UIColor.Gray;

            nfloat h = 40f;
            nfloat w = View.Bounds.Width;
           
            usernameField = new UITextField
            {
                Placeholder = "Enter your username",
                BorderStyle = UITextBorderStyle.RoundedRect,
                Frame = new CGRect(10, 100, w - 20, h)
            };
            passwordField = new UITextField
            {
                Placeholder = "Enter your password",
                BorderStyle = UITextBorderStyle.RoundedRect,
                Frame = new CGRect(10, 150, w - 20, h),
                SecureTextEntry = true
            };
            var submitButton = UIButton.FromType(UIButtonType.RoundedRect);

            submitButton.Frame = new CGRect(10, 200, w - 20, 44);
            submitButton.SetTitle("Submit", UIControlState.Normal);
            
            submitButton.BackgroundColor = UIColor.White;
            View.AddSubview(usernameField);
            View.AddSubview(passwordField);
            View.AddSubview(submitButton);

            submitButton.TouchUpInside += (sender, e) => SubmitUser();
        }

        private void SubmitUser()
        {
            User newUser;
            if (Submit_Validate())
            {
                //todo connect to json writer
                newUser = new User()
                {
                    Id = 5,
                    Username = usernameField.Text,
                    Password = "******"
                };
                view.users.Add(newUser);

                this.NavigationController.PopToRootViewController(true);

            }
        }

        private bool Submit_Validate()
        {
            //todo write validation
            List<string> redText = new List<string> { };
            bool isValid = true;

            if (string.IsNullOrWhiteSpace(usernameField.Text))
            {
                redText.Add("Please Enter a Username");
                isValid = false;
            }
            else if (passwordField.Text.Length <5 || passwordField.Text.Length >12)
            {
                redText.Add("Password must be between 5 and 12 characters");
                isValid = false;
            }

            else if (!Regex.IsMatch(passwordField.Text, @"^[a-zA-Z0-9]+$") )
            {
                redText.Add("Password must consist of only numbers and letters. No special characters!");
                isValid = false;
            }

            else if (!(passwordField.Text.Any(Char.IsLetter)) )
            {
                redText.Add("Password must have at least one letter ");
                isValid = false;
            }
            else if ( !passwordField.Text.Any(Char.IsNumber))
            {
                redText.Add("Password must have at least one number");
                isValid = false;
            }

            //todo finish this validation
            //else if (!Regex.IsMatch(passwordField.Text, @"^(?!.*(?<g>[a-z\d]+)\k<g>.*)[a-z\d]{5,12}(?<=.*[a-z].*)(?<=.*\d.*)$"))
            //{
            //    redText.Add("Password cannot contain repeating phrases");
            //    isValid = false;
            //}
            if (isValid == false)
            {
                UIAlertController okAlertController = UIAlertController.Create("Something went wrong",string.Join("\n",redText),
                    UIAlertControllerStyle.Alert);
                okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                this.PresentViewController(okAlertController, true, null);
            }
            return isValid;
        }
    }
}